
describe('workspace-project App', () => {

    beforeEach(() => {
	cy.visit('/');
    })

    it('successfully loads', () => {
    })
    
    it('should display welcome message', () => {
	cy.get('app-root').get('h1').should('contain','Cartolabe');
  });
});
