// export for convenience.
/* tslint:disable: directive-selector use-host-property-decorator component-selector no-input-rename */
export { ActivatedRoute, Router, RouterLink, RouterOutlet } from '@angular/router';
import { convertToParamMap, ParamMap, Params } from '@angular/router';

import { Component, Directive, Injectable, Input, HostListener } from '@angular/core';
import { NavigationExtras } from '@angular/router';

// #docregion router-link
@Directive({
  selector: '[routerLink]',
})
export class RouterLinkStubDirective {
  @Input('routerLink') linkParams: any;
  navigatedTo: any = null;

  @HostListener('click') onClick() {
    this.navigatedTo = this.linkParams;
  }
}
// #enddocregion router-link
@Component({ selector: 'router-outlet', template: '' })
export class RouterOutletStubComponent { }

export class RouterStub {
  navigate(commands: any[], extras?: NavigationExtras) { }
}


// Only implements params and part of snapshot.params
// #docregion activated-route-stub
import { BehaviorSubject } from 'rxjs';

/**
 * An ActivateRoute test double with a `paramMap` observable.
 * Use the `setParamMap()` method to add the next `paramMap` value.
 */
@Injectable()
export class ActivatedRouteStub {

  // ActivatedRoute.params is Observable
  private params$ = new BehaviorSubject<ParamMap>(null);
  private data$ = new BehaviorSubject(this.testData);
  private queryParams$ = new BehaviorSubject(this.mockQueryParams);
  private fragment$ = new BehaviorSubject<string>('');
  readonly paramMap = this.params$.asObservable();
  readonly queryParams = this.queryParams$.asObservable();
  readonly fragment = this.fragment$.asObservable();
  data = this.data$.asObservable();

  constructor(initialParams?: Params) {
    this.testParams = initialParams;
  }

  // Parent route is this route
  get parent() { return this; }

  // Test parameters
  private _testParams: ParamMap;
  get testParams() { return this._testParams; }
  /** Set the paramMap observables's next value */
  set testParams(params: Params) {
    this._testParams = convertToParamMap(params);
    this.params$.next(this._testParams);
  }

  // Query parameters
  private _queryParams: {};
  get mockQueryParams() { return this._queryParams; }
  set mockQueryParams(params: {}) {
    this._queryParams = params;
    this.queryParams$.next(params);
  }

  // Test dataset
  private _testData: {};
  get testData() { return this._testData; }
  set testData(data: {}) {
    this._testData = data;
    this.data$.next(data);
  }

  // ActivatedRoute.snapshot.params
  get snapshot() {
    return { paramMap: this.testParams, fragment: this.testFragment };
  }

  // Fragment
  private _testFragment: string;
  get testFragment() { return this._testFragment; }
  set testFragment(fragment: string) {
    this._testFragment = fragment;
    this.fragment$.next(fragment);
  }
}

