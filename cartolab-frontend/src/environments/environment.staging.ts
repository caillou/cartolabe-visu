export const environment = {
  production: true,
  apiEndpoint: 'https://cartolabe-beta.lisn.upsaclay.fr/api/v1',
  staticEndpoint: 'https://cartolabe-beta.lisn.upsaclay.fr/static',
  defaultDatasetKey: 'lisn',
  contactEmail: 'cartolabe@lisn.upsaclay.fr',
  appendGA: true
};
