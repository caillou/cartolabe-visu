import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
import { RouterLinkStubDirective } from 'src/testing/router.stubs';
import { AuthServiceStub } from 'src/testing/services.stubs';

import { AuthService } from 'src/app/auth/services/auth.service';
import { NavbarComponent } from './navbar.component';
import { User } from 'src/app/auth/models/user';
import { Dataset } from 'src/app/datasets/models/dataset';
import { DatasetsStore } from 'src/app/datasets/store';

describe('NavbarComponent', () => {
    let component: NavbarComponent;
    let fixture: ComponentFixture<NavbarComponent>;
    let links: RouterLinkStubDirective[];
    let linkDes: DebugElement[];
    let authService: AuthService;
    let dsStore: DatasetsStore;
    const mockDatasets = [{ id: 1, label: 'Inria', key: 'inria',
			    version:'1.0.0', current: '1.0.0' },
			  { id: 2, label: 'LRI', key: 'lri',
			  version:'1.0.0', current: '1.0.0'}] as Dataset[];

    beforeEach(() => {
	TestBed.configureTestingModule({
	    imports: [
	    ],
	    declarations: [
		NavbarComponent,
		RouterLinkStubDirective
	    ],
	    providers: [
		{ provide: AuthService, useClass: AuthServiceStub },
	    ]
	}).compileComponents();
	authService = TestBed.get(AuthService);
	dsStore = TestBed.get(DatasetsStore);
	dsStore.setDatasets(mockDatasets);
	fixture = TestBed.createComponent(NavbarComponent);
	component = fixture.componentInstance;
	fixture.detectChanges();
	
	// find DebugElements with an attached RouterLinkStubDirective
	linkDes = fixture.debugElement.queryAll(By.directive(RouterLinkStubDirective));
	// get the attached link directive instances using the DebugElement injectors
	links = linkDes.map(de => de.injector.get(RouterLinkStubDirective) as RouterLinkStubDirective);
    });

    it('should create', () => {
	expect(component).toBeTruthy();
    });
    
    it('can get RouterLinks from template', () => {
	expect(links.length).toBe(3, 'should have 3 links');
	// expect(links[0].linkParams[0]).toEqual('/datasets', '1st link should go to Datasets');
	expect(
	    links[0].linkParams
	).toEqual(['/map', 'inria', '1.0.0'], '1st link should go to Inria Map');
	expect(
	    links[1].linkParams
	).toEqual(['/map', 'lri', '1.0.0'], '2nd link should go to Lri Map');
	expect(links[2].linkParams).toEqual(['/about'], '3rd link should go to About Page');
    });

    it('can click Map link in template', () => {
	const mapLinkDe = linkDes[0];
	const mapLink = links[0];
	
	expect(mapLink.navigatedTo).toBeNull('link should not have navigated yet');
	
	mapLinkDe.triggerEventHandler('click', null);
	fixture.detectChanges();
	
	expect(mapLink.navigatedTo).toEqual(['/map', 'inria', '1.0.0']);
    });
    
    describe('logout button', () => {
	it('should not be visible when user not logged in', () => {
	    const btn = fixture.debugElement.query(By.css('button.logout'));
	    expect(btn).toBeFalsy();
	});
	
	it('should be visible when user exists', () => {
	    authService.loggedUser.next(new User());
	    fixture.detectChanges();
	    const btn = fixture.debugElement.query(By.css('button.logout'));
	    expect(btn).toBeTruthy();
	});
	
	it('should call authService.logout when clicked', () => {
	    authService.loggedUser.next(new User());
	    fixture.detectChanges();
	    const btn = fixture.debugElement.query(By.css('button.logout'));
	    
	    btn.nativeElement.click();
	    
	    expect(authService.logout).toHaveBeenCalled();
	});
    });
});
