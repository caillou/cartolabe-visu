import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { ToastrService } from 'ngx-toastr';
import { throwError, of } from 'rxjs';
import { AuthServiceStub } from 'src/testing/services.stubs';
import { ActivatedRouteStub, ActivatedRoute } from 'src/testing/router.stubs';
import { LoginComponent } from './login.component';
import { AuthService } from 'src/app/auth/services/auth.service';
import { User } from 'src/app/auth/models/user';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let authService: AuthService;
  let activatedRoute: ActivatedRouteStub;
  let toastrStub: Partial<ToastrService>;
  let toastr: ToastrService;
  const routerSpy = jasmine.createSpyObj('Router', ['navigateByUrl']);
  let router: Router;

  beforeEach(async(() => {
    toastrStub = {
      success: jasmine.createSpy('success'),
      error: jasmine.createSpy('error')
    };
    activatedRoute = new ActivatedRouteStub();
    activatedRoute.mockQueryParams = { returnUrl: '/datasets' };

    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule
      ],
      declarations: [LoginComponent],
      providers: [
        { provide: ActivatedRoute, useValue: activatedRoute },
        { provide: AuthService, useClass: AuthServiceStub },
        { provide: ToastrService, useValue: toastrStub },
        { provide: Router, useValue: routerSpy },
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    authService = TestBed.get(AuthService);
    router = TestBed.get(Router);
    toastr = TestBed.get(ToastrService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('login', () => {
    it('should display an error message if form is invalid', () => {
      const btn = fixture.debugElement.query(By.css('.btn-primary'));
      expect(btn).toBeTruthy();
      btn.nativeElement.click();
      fixture.detectChanges();

      expect(toastr.error).toHaveBeenCalledWith('Please fill in the required fields to login.');
      expect((<jasmine.Spy>authService.login).calls.any()).toBe(false);
    });

    it('should display an error message if login failed', () => {
      (<jasmine.Spy>authService.login).and.returnValue(throwError('An error occured logging in.'));
      component.loginForm.controls['email'].setValue('admin@lri.fr');
      component.loginForm.controls['password'].setValue('password');
      const btn = fixture.debugElement.query(By.css('.btn-primary'));
      btn.nativeElement.click();
      fixture.detectChanges();

      expect(authService.login).toHaveBeenCalledWith('admin@lri.fr', 'password');
      expect(toastr.error).
        toHaveBeenCalledWith('Unable to login with the information provided. Please check that you entered your credentials correctly.');
    });

    it('should navigate to redirect url on success', () => {
      (<jasmine.Spy>authService.login).and.returnValue(of(new User()));
      component.loginForm.controls['email'].setValue('admin@lri.fr');
      component.loginForm.controls['password'].setValue('password');
      const btn = fixture.debugElement.query(By.css('.btn-primary'));
      btn.nativeElement.click();
      fixture.detectChanges();

      expect(authService.login).toHaveBeenCalledWith('admin@lri.fr', 'password');
      expect(router.navigateByUrl).toHaveBeenCalledWith('/datasets');
    });
  });
});
