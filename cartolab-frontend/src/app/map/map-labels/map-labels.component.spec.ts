import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { select } from 'd3-selection';
import { zoomIdentity } from 'd3-zoom';
import { MapServiceStub } from 'src/testing/services.stubs';
import { MapLabelsComponent } from './map-labels.component';
import { MapService } from 'src/app/map/services';
import { Dataset } from 'src/app/datasets/models';
import { DatasetsStore } from 'src/app/datasets/store';
import { MapStore } from 'src/app/map/store';
import { MapProperties, Point, MapViewboxBounds } from 'src/app/map/models';

describe('MapLabelsComponent', () => {
    let component: MapLabelsComponent;
    let fixture: ComponentFixture<MapLabelsComponent>;
    let mapService: MapService;
    let dsStore: DatasetsStore;
    let mapStore: MapStore;
    const props = { width: 600, height: 600 } as MapProperties;
    const datasets = [
	{
	    id: 1, label: 'Inria', key: 'inria', version: '1.0.0',
	    current: '1.0.0',description: 'Desc',
	    natures: [
		{
		    id: 101, label: 'Articles', key: 'articles', checked: true,
		    limit: 0, color: 'blue', icon: 'subject', tooltip: '',
		    layer: true, inactive: false, layer_tooltip: ''
		},
		{
		    id: 102, label: 'Authors', key: 'authors', checked: true,
		    limit: 0, color: 'red', icon: 'person', tooltip: '',
		    layer: true, inactive: false, layer_tooltip: ''
		}
	    ]
	},
	{
	    id: 2, label: 'Hal', key: 'hal', version: '1.0.0',
	    current: '1.0.0', description: 'Desc',
	    metadata: { 'coords_range': [-20, 20] },
	    natures:
            [
		{
		    id: 101, label: 'Articles', key: 'articles', checked: true,
		    limit: 0, color: 'blue', icon: 'subject', tooltip: '',
		    layer: true, inactive: false, layer_tooltip: ''
		},
		{
		    id: 102, label: 'Authors', key: 'authors', checked: true,
		    limit: 0, color: 'red', icon: 'person', tooltip: '',
		    layer: true, inactive: false, layer_tooltip: ''
		}
            ]
	}
    ] as undefined as Dataset[];
    
    const mockPoints = [
	{ id: 'abc-12-df', score: 10, position: [1.0, 2.0],
	  label: 'Article', nature: 'articles' },
	{ id: 'deg-345-gh', score: 5, position: [-5.2, -3.0],
	  label: 'Author', nature: 'authors' },
	{ id: 'uio-34-rzh', score: 40, position: [12.5, 6.8],
	  label: 'Team', nature: 'teams' },
	{ id: 'tio-789-dfj', score: 50, position: [5.9, -12.8],
	  label: 'Lab', nature: 'labs' }
    ] as Point[];

    beforeEach(() => {
	TestBed.configureTestingModule({
	    declarations: [MapLabelsComponent],
	    providers: [
		{ provide: MapService, useClass: MapServiceStub },
	    ]
	}).compileComponents();
	mapService = TestBed.get(MapService);
	mapStore = TestBed.get(MapStore);
	dsStore = TestBed.get(DatasetsStore);
	dsStore.setDatasets(datasets);
	dsStore.setDataset(datasets[0].key, datasets[0].version);

	fixture = TestBed.createComponent(MapLabelsComponent);
	component = fixture.componentInstance;
	component.props = props;
	fixture.detectChanges();
    });
    
    it('should create', () => {
	expect(component).toBeTruthy();
    });
    
    describe('setupForDataset', () => {
	let naturesSpy: jasmine.Spy;

	beforeEach(() => {
	    naturesSpy = spyOnProperty(dsStore, 'natures', 'get').and.callThrough();
	});

	it('should not run if dataset has no coords_range', () => {
	    component.initSvg();
	    expect(naturesSpy.calls.any()).toBe(false);
	});
	
	it('should run when dataset has coords_range', () => {
	    dsStore.setDataset(datasets[1].key, datasets[1].version);
	    component.initSvg();
	    expect(naturesSpy).toHaveBeenCalled();
	});
    });
    
    describe('zoomed', () => {
	it('should call drawLabels and trigger viewBox update on service', (done) => {
	    component.initSvg();
	    dsStore.setDataset(datasets[1].key, datasets[1].version);
	    const spy = spyOn(component, 'drawLabels');

	    component.zoomed(zoomIdentity);
	    expect(spy).toHaveBeenCalled();
	    fixture.whenStable().then(() => {	
  		expect(mapService.updateViewBox).toHaveBeenCalled();
  		done();
	    });
	});
    });

    describe('viewBoxPoints$', () => {
	let layoutLabelsSpy: jasmine.Spy;
	
	beforeEach(() => {
	    layoutLabelsSpy = spyOn(component, 'layoutLabels');
	});
	
	it('should call layoutLabel when new points are received', () => {
	    mapStore.addViewBoxPoints(mockPoints);
	    expect(layoutLabelsSpy).toHaveBeenCalled();
	});
    });

    describe('selectedPoint$', () => {
	let drawLabelsSpy: jasmine.Spy;
	
	beforeEach(() => {
	    drawLabelsSpy = spyOn(component, 'drawLabels');
	});
	
	it('should call drawLabels', () => {
	    mapStore.setSelectedPoint(mockPoints[0]);
	    expect(drawLabelsSpy).toHaveBeenCalled();
	});
    });
    
    describe('searchPoints$', () => {
	let layoutLabelsSpy: jasmine.Spy;
	
	beforeEach(() => {
	    layoutLabelsSpy = spyOn(component, 'layoutLabels');
	});
	
	it('should call layoutLabels and fitZoomToSearch', () => {
	    mapStore.setSearchPoints(mockPoints, 'Search results');
	    expect(layoutLabelsSpy).toHaveBeenCalled();
	});
    });
    
    describe('clickInput$', () => {
	it('should call searchInBounds on mapService after delay', (done) => {
	    const bounds = new MapViewboxBounds([1.23, 2.54], [2.34, -9.12]);
	    component.clickInput$.next(bounds);
	    fixture.whenStable().then(() => {
		expect(mapService.searchInBounds).toHaveBeenCalledWith(bounds);
		done();
	    });  
	});
    });

    describe('viewBoxInput$', () => {
	it('should call updateViewBox on mapService after delay', (done) => {
	    const bounds = new MapViewboxBounds([1.23, 2.54], [2.34, -9.12]);
	    component.viewBoxInput$.next(bounds);
	    fixture.whenStable().then(() => {
		expect(mapService.updateViewBox).toHaveBeenCalledWith(bounds, true);
		done();
	    });
	});
    });

    describe('layoutLabels', () => {
	let selectPointSpy: jasmine.Spy;
	beforeEach(() => {
	    selectPointSpy = spyOn(mapStore, 'setSelectedPoint');
	    component.initSvg();
	    dsStore.setDataset(datasets[1].key, datasets[1].version);
	});

	it('should draw labels', () => {
	    // add points to store
	    component.zoomed(zoomIdentity);
	    mapStore.addViewBoxPoints(mockPoints);
	    fixture.detectChanges();
	    
	    // Expect 4 labels in the svg
	    const svg = fixture.debugElement.query(By.css('svg.labels-svg'));
	    const g = svg.nativeElement.getElementsByClassName('label');
	    expect(g.length).toBe(4);
	});

	it('should set a click listener for labels', () => {
	    component.zoomed(zoomIdentity);
	    mapStore.addViewBoxPoints(mockPoints);
	    fixture.detectChanges();
	    
	    const sel = select('.label.authors');
	    sel.dispatch('click');
	    
	    fixture.detectChanges();
	    expect(selectPointSpy).toHaveBeenCalledWith(mockPoints[1]);
	});
    });
    
    describe('drawClusters', () => {
	beforeEach(() => {
	    component.initSvg();
	    dsStore.setDataset(datasets[1].key, datasets[1].version);
	});

	it('should add a cluster path for each cluster point', () => {
	    component.zoomed(zoomIdentity);
	    mapStore.setClusters(mockPoints);
	    
	    // Expect 4 themes in the svg
	    const svg = fixture.debugElement.query(By.css('svg.labels-svg'));
	    const g = svg.nativeElement.getElementsByClassName('theme');
	    expect(g.length).toBe(4);
	    mockPoints.forEach((point, idx) => expect(
		g[idx].getAttribute('id')).toEqual('theme' + point.id)
			      );
	});

	it('should remove cluster paths if there are less than 3 clusters', () => {
	    component.zoomed(zoomIdentity);
	    mapStore.setClusters(mockPoints);
	    
	    // Expect 4 themes in the svg
	    const svg = fixture.debugElement.query(By.css('svg.labels-svg'));
	    let g = svg.nativeElement.getElementsByClassName('theme');
	    expect(g.length).toBe(4);
	    
	    mapStore.resetViewBoxPoints();
	    mapStore.setClusters([]);
	    mapStore.addViewBoxPoints(mockPoints);
	    fixture.detectChanges();
	    
	    g = svg.nativeElement.getElementsByClassName('theme');
	    expect(g.length).toBe(0);
	});
	
	it('should not redraw clusters if the number of clusters has not changed', () => {
	    // Draw Clusters
	    component.zoomed(zoomIdentity);
	    mapStore.setClusters(mockPoints);
	    // Change cluster ids
	    const ids = mockPoints.map(p => p.id);
	    mockPoints.forEach((p, i) => p.id = String(i));
	    // Redraw Clusters
	    mapStore.setClusters(mockPoints);
	    // Expect ids to not have changed
	    const svg = fixture.debugElement.query(By.css('svg.labels-svg'));
	    const g = svg.nativeElement.getElementsByClassName('theme');
	    expect(g.length).toBe(4);
	    ids.forEach((id, i) => expect(
		g[i].getAttribute('id')).toEqual('theme' + id)
		       );
	});
    });

});

