import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule } from '@angular/forms';
import { MatIconModule} from '@angular/material/icon';
import { MatTooltipModule } from '@angular/material/tooltip';
import { By } from '@angular/platform-browser';
import { MapControlComponent, PointListComponent, SearchBoxComponent } from 'src/testing/components.stubs';
import { DatasetsStore } from 'src/app/datasets/store';
import { Dataset, Nature } from 'src/app/datasets/models';
import { DashboardComponent } from './dashboard.component';



describe('DashboardComponent', () => {
    let component: DashboardComponent;
    let fixture: ComponentFixture<DashboardComponent>;
    let dsStore: DatasetsStore;
    let mockDatasets: Dataset[];
    let mockNatures: Array<Nature>;

    beforeEach(() => {
	mockNatures = [
	    { label: 'Articles', key: 'articles', checked: true, limit: 2,
	      color: '#0288d1', layer: true, inactive: false, icon: 'subject' },
	    { label: 'Authors', key: 'authors', checked: true, limit: 30,
	      color: '#e64a19', layer: true, inactive: false, icon: 'person' },
	    { label: 'Teams', key: 'teams', checked: true, limit: 30,
	      color: '#388e3c', layer: false, inactive: false },
	    { label: 'Labs', key: 'labs', checked: true, limit: 25,
	      color: '#009624', layer: false, inactive: false }
	] as Nature[];
	
	mockDatasets = [
	    { id: 1, label: 'Inria', key: 'inria', version: '1.0.0',
	      current: '1.0.0',
	      description: 'Projection of articles for INRIA',
	      natures: mockNatures },
	    { id: 2, label: 'LRI', key: 'lri', version: '1.0.0',
	      current: '1.0.0' }] as Dataset[];

	TestBed.configureTestingModule({
	    imports: [
		FormsModule,
		HttpClientTestingModule,
		MatIconModule,
		MatTooltipModule,
		RouterTestingModule
	    ],
	    declarations: [
		DashboardComponent,
		MapControlComponent,
		PointListComponent,
		SearchBoxComponent
	    ]
	}).compileComponents();
	
	dsStore = TestBed.get(DatasetsStore);
	dsStore.setDatasets(mockDatasets);
	dsStore.setDataset(mockDatasets[0].key, mockDatasets[0].version);
	fixture = TestBed.createComponent(DashboardComponent);
	component = fixture.componentInstance;
	fixture.detectChanges();
    });

    it('should create', () => {
	expect(component).toBeTruthy();
    });
    
    it('should display the dataset description', () => {
	const descDe = fixture.debugElement.query(By.css('.map-title'));
	expect(descDe.nativeElement.textContent.trim()).toEqual(
	    mockDatasets[0].description + ' Version ' + mockDatasets[0].version
	);
    });
    
    it('should display two layers', () => {
	const layersDe = fixture.debugElement.queryAll(By.css('.map-keys .layer-item'));
	expect(layersDe.length).toBe(2);
	
	expect(
	    layersDe[0].nativeElement.textContent.trim()
	).toEqual('subject Articles');
	expect(
	    layersDe[1].nativeElement.textContent.trim()
	).toEqual('person Authors');
    });

    it('should call toggleLayer when a layer is selected', () => {
	const layersDe = fixture.debugElement.queryAll(By.css('.map-keys .layer-item'));
	expect(layersDe.length).toBe(2);
	spyOn(dsStore, 'toggleLayer');
	
	layersDe[0].nativeElement.dispatchEvent(new Event('click'));
	
	fixture.detectChanges();
	expect(dsStore.toggleLayer).toHaveBeenCalledWith('articles');
    });
    
    it('should display the labels', () => {
	let pointsDe = fixture.debugElement.queryAll(By.css('.label-item'));
	expect(pointsDe.length).toBe(4);
	
	expect(
	    pointsDe[0].nativeElement.textContent.trim()
	).toEqual('subject Articles');
	expect(
	    pointsDe[1].nativeElement.textContent.trim()
	).toEqual('person Authors');
	expect(
	    pointsDe[2].nativeElement.textContent.trim()
	).toEqual('Teams');
	expect(
	    pointsDe[3].nativeElement.textContent.trim()
	).toEqual('Labs');

	pointsDe = fixture.debugElement.queryAll(By.css('.cluster-item'));
	expect(pointsDe.length).toBe(1);
	expect(
	    pointsDe[0].nativeElement.textContent.trim()
	).toEqual('format_shapes Clusters');
    });
});

