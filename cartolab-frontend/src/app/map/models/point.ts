export class Point {
  id: string;
  carto_id: number;
  score: number;
  rank: number;
  nature: string;
  label: string;
  url: string;
  position: number[];
  img_data: string;
  location: string;
  is_search_result: boolean;
  is_selected: boolean;
  is_cluster: boolean;

  toString() {
    // IMPORTANT: this method is used for object equality by TypeScript Collections.
    return this.id;
  }
}
