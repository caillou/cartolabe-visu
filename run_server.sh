#!/bin/bash

sleep 60

curl -XPUT $ELASTIC_HOST/_template/all_indices -H 'Content-Type: application/json' -d '
{
  "template": "*",
  "settings": {
    "refresh_interval": "60s"
  }
}'

for DUMP in "$@"; do

    echo "####################################################################"
    echo "Starting to index and tile datasets. This may take some time..."
    echo "####################################################################"

    echo "####################################################################"
    echo "Processing $DUMP dataset."
    echo "####################################################################"
    python manage.py loaddata $DUMP

    echo "####################################################################"
    echo "Downloading $DUMP..."
    echo "####################################################################"

    python manage.py download --dataset_key=$DUMP

    echo "####################################################################"
    echo "Creating index for $DUMP..."
    echo "####################################################################"

    python manage.py index --dataset_key=$DUMP --dump_dir=$DUMP

    sleep 90

    echo "####################################################################"
    echo "Creating tiles for $DUMP..."
    echo "####################################################################"
    
    python manage.py tile --dataset_key=$DUMP

done

curl -XPUT $ELASTIC_HOST/_template/all_indices -H 'Content-Type: application/json' -d '
{
  "template": "*",
  "settings": {
    "refresh_interval": "1s"
  }
}'

echo "####################################################################"
echo ""
echo "Cartolabe-backend is ready!"
echo ""
echo "Server is available at http://localhost:4200/map/$defaultDatasetKey"
echo ""
echo "####################################################################"

python manage.py runserver 0.0.0.0:8000
