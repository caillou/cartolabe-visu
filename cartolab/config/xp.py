"""
Production machine configuration.
"""

from .common_prod import *  # noqa

ALLOWED_HOSTS = ['localhost',
                 'cartovm-xp.lisn.upsaclay.fr',
                 'cartolabe-xp.lisn.upsaclay.fr']

INTERNAL_IPS = ['127.0.0.1']

CORS_ORIGIN_WHITELIST = ('http://localhost',
                         'http://cartovm-xp.lisn.upsaclay.fr',
                         'http://cartolabe-xp.lisn.upsaclay.fr'
                         )
