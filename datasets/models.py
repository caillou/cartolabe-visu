import json

from django.conf import settings
from django.db import models
from elasticsearch_dsl import (Document, Integer, Float, Text, InnerDoc,
                               Nested, GeoPoint, Keyword)


class Dataset(models.Model):
    label = models.CharField(max_length=100, unique=False, null=False,
                             blank=False)
    key = models.CharField(max_length=100, unique=True, null=False,
                           blank=False)
    description = models.TextField(null=True, blank=True, default=None)
    about_description = models.TextField(null=True, blank=True, default=None)
    analysed_on = models.DateTimeField(blank=True, null=True, editable=False)
    tiled_on = models.DateTimeField(blank=True, null=True, editable=False)
    max_zoom = models.IntegerField(null=True, blank=True)
    metadata = models.TextField(null=True, blank=False,
                                default='{"coords_range": [-20,20]}')
    working = models.BooleanField(null=False, default=False)
    private = models.BooleanField(null=False, default=False)
    url = models.URLField(null=True, blank=False)
    version = models.TextField(null=False, blank=False, default="1.0.0")

    def __repr__(self):
        return '<Dataset(key=%s)>' % self.key

    def __str__(self):
        return '%s (%s)' % (self.label, self.key)

    def set_metadata(self, metadata):
        self.metadata = json.dumps(metadata)

    def get_metadata(self):
        if self.metadata:
            return json.loads(self.metadata)
        return dict()

    def update_metadata(self, new_metadata):
        metadata = self.get_metadata()
        metadata.update(new_metadata)
        self.set_metadata(metadata)

    def patch_metadata(self, key, value):
        metadata = self.get_metadata()
        metadata[key] = value
        self.set_metadata(metadata)

    @property
    def private(self):
        return self.key in settings.PRIVATE_DATASET_KEYS


class Nature(models.Model):
    label = models.CharField(max_length=100, null=False, blank=False)
    key = models.CharField(max_length=100, null=False, blank=False)
    checked = models.BooleanField(default=True)
    layer = models.BooleanField(default=False)
    limit = models.IntegerField(default=0, null=False, blank=False)
    color = models.CharField(max_length=100)
    icon = models.CharField(max_length=100)
    tooltip = models.CharField(max_length=500)
    layer_tooltip = models.CharField(max_length=500, null=True, blank=True)
    dataset = models.ForeignKey(
        Dataset, on_delete=models.CASCADE, related_name='natures')

    def __str__(self):
        return '%s - %s' % (self.dataset, self.key)


class ExtraField(models.Model):
    label = models.CharField(max_length=100, null=False, blank=False)
    key = models.CharField(max_length=100, null=False, blank=False)
    tooltip = models.CharField(max_length=500, null=True, blank=True)
    nature = models.ForeignKey(
        Nature, on_delete=models.CASCADE, related_name='extra_fields')

    def __str__(self):
        return '%s - %s' % (self.nature, self.key)


class Neighbors(InnerDoc):
    nature = Text(required=True)
    ids = Keyword(required=True, multi=True)


class Point(Document):
    """
    Point Document mapping for elasticsearch
    """
    position = GeoPoint(
        required=True)  # The 2D coordinates on the Cartolabe map
    score = Float(required=True)
    rank = Integer()
    nature = Keyword(required=True)
    label = Text(required=True)
    url = Keyword(multi=False)
    img_data = Keyword(multi=False)
    neighbors = Nested(Neighbors)
    location = GeoPoint(required=False)  # Lat,Long GPS position

    def add_neighbors(self, nature, neighbors):
        self.neighbors.append(Neighbors(nature=nature, ids=neighbors))
