import os

from django.conf import settings
from django.core.management.base import BaseCommand, CommandError
from django.utils.timezone import now

from datasets.indexing import import_for_dataset
from datasets.models import Dataset


class Command(BaseCommand):
    help = 'Import data from an export.json file into ElasticSearch.'

    def add_arguments(self, parser):
        parser.add_argument('--dump_dir',
                            help=('The name of the dump directory containing'
                                  ' the export.json/export.feather file.')
                            )
        parser.add_argument('--dataset_key',
                            help='The dataset key to import the data into.')
        parser.add_argument('--dataset_version',
                            help='The dataset version to process.')

    def handle(self, *args, **options):
        dump_dir = options['dump_dir']
        if dump_dir is None:
            dump_dir = self.get_dump_dir()

        dataset_key = options['dataset_key']
        if dataset_key is None:
            dataset_key = self.get_dataset_key()

        version = options['dataset_version']

        dataset = Dataset.objects.get(key=dataset_key)
        if version is None:
            version = dataset.version
        else:
            dataset.version = version
        work_dir = os.path.join(dump_dir, dataset_key, version)

        self.index_dataset(dataset, work_dir)

    def index_dataset(self, dataset, work_dir):
        export_file = self.get_export_file(work_dir)
        max_coord = import_for_dataset(export_file, dataset)

        dataset.analysed_on = now()
        dataset.update_metadata({'coords_range': [-max_coord, max_coord]})
        versions = dataset.get_metadata().get('versions', [dataset.version])
        versions.append(dataset.version)
        versions = set(versions)
        dataset.update_metadata({'versions': list(versions)})
        dataset.working = False
        dataset.save()
        self.delete_dataset_groups(dataset)

    def delete_dataset_groups(self, dataset):
        """
        Delete the dataset's groups from the database. Deletes only the groups
        created for dataset version that is being indexed. The pre_delete signal
        will also delete the group's files on disk.
        """

        groups = dataset.groups.filter(version=dataset.version)

        if groups is not None:
            for group in groups:
                if group.version == dataset.version:
                    group.delete()

    def get_export_file(self, work_dir):
        extensions = ['.json', '.feather']
        files = []
        for ext in extensions:
            file = 'export' + ext
            if os.path.isfile(os.path.join(work_dir, file)):
                files.append(file)
        if len(files) == 1:
            return os.path.join(work_dir, files[0])

        file = self.get_input_data(
            'Which export file to use ({}):'.format(', '.join(files)))
        return os.path.join(work_dir, file)

    def get_dataset_key(self):
        keys = list(Dataset.objects.all().values_list('key', flat=True))
        return self.get_input_data('Dataset to import the data into ({}): '.format(', '.join(keys)))

    def get_dataset_version(self):
        return self.get_input_data('Dataset to import the data into ({}): ',
                                   "1.0.0")

    def get_dump_dir(self):
        if not os.path.isdir(settings.DATASETS_DUMP_DIR):
            raise CommandError('The dump directory {} does not exist.'.format(
                settings.DATASETS_DUMP_DIR))

        return self.get_input_data('Read export.json file from ({}): '.
                                   format(', '.join(next(os.walk(settings.DATASETS_DUMP_DIR))[1])))

    def get_input_data(self, message, default=None):
        """
        Override this method if you want to customize data inputs or
        validation exceptions.
        """
        raw_value = input(message)
        if default and raw_value == '':
            raw_value = default

        return raw_value
