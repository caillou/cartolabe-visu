from django.conf import settings
from elasticsearch_dsl import FacetedSearch, TermsFacet, Search, Mapping
from rest_framework import generics, permissions
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import BasePermission
from rest_framework.response import Response
from rest_framework.views import APIView

from datasets.models import Dataset
from datasets.models import Point
from datasets.serializers import DatasetSerializer


class PrivateDatasetPermission(BasePermission):
    """
    Only allows access to authenticated users if request dataset key in
    PRIVATE_DATASET_KEYS
    """

    def has_permission(self, request, view):
        key = view.kwargs.get('key', None)
        return key not in settings.PRIVATE_DATASET_KEYS or bool(
            request.user and request.user.is_authenticated
        )


class DatasetList(generics.ListCreateAPIView):
    queryset = Dataset.objects.all()
    serializer_class = DatasetSerializer
    permission_classes = (permissions.AllowAny,)


class DatasetDetailView(generics.RetrieveAPIView):
    queryset = Dataset.objects.all()
    serializer_class = DatasetSerializer
    permission_classes = (permissions.AllowAny,)

    def get_object(self):
        queryset = self.get_queryset()
        if 'pk' in self.kwargs:
            pk = self.kwargs['pk']
            dataset = get_object_or_404(queryset, pk=pk)
        else:
            key = self.kwargs['key']
            dataset = get_object_or_404(queryset, key=key)

        self.check_object_permissions(self.request, dataset)
        return dataset


class DatasetNaturesSearch(FacetedSearch):
    doc_types = Point

    facets = {
        # use bucket aggregations to define facets
        'natures': TermsFacet(field='nature')
    }

    def __init__(self, index):
        super().__init__()
        self.index = index


class DatasetStatsView(APIView):
    """
    Get ElasticSearch stats for a dataset
    """
    permission_classes = (PrivateDatasetPermission,)

    def get_queryset(self):
        """Get Queryset."""

        index_name = self.kwargs['key'] + "-" + self.kwargs['version']
        queryset = Search(index=index_name, doc_type=Point)
        return queryset

    def get(self, request, key, version):

        index_name = key + '-' + version
        Mapping.from_es(index_name)

        s = self.get_queryset()
        s.aggs.bucket('natures', 'terms', field='nature')
        response = s.execute()
        buckets = {
            b.key: b.doc_count for b in response.aggregations.natures.buckets
        }
        return Response(buckets)
